package com.lecuong.shop.security;

import com.lecuong.shop.security.jwt.model.JWTPayload;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;

public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

    private HashSet<GrantedAuthority> authorities = new HashSet();
    private JWTPayload user;

    public UserDetails() {
        this.authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    public String getPassword() {
        return "password";
    }

    public String getUsername() {
        return this.user.getUserName();
    }

    public JWTPayload getUser() {
        return this.user;
    }

    void setUser(JWTPayload user) {
        this.user = user;
        String[] roles = user.getRole().split(",");
        for (String role : roles) {
            this.authorities.add(new SimpleGrantedAuthority(role));
        }
    }
}
