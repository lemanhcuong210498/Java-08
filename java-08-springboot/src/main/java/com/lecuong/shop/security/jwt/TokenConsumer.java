package com.lecuong.shop.security.jwt;

import com.lecuong.shop.security.jwt.model.JWTPayload;
import com.lecuong.shop.security.jwt.util.JWTClaimKey;
import org.jose4j.jwt.consumer.InvalidJwtException;

import java.security.PublicKey;
import java.util.Map;

public class TokenConsumer {

    //Nhận về 1 token và chuyển token đó sang dạng map

    private String audience;
    private JWTParser parser;


    public TokenConsumer(String audience, PublicKey publicKey) {
        this.audience = audience;
        parser = new JWTParser(publicKey);
    }

    public JWTPayload consume(String token) throws InvalidJwtException {
        Map<String, Object> claims = parser.parseToken(token, audience);

        JWTPayload jwtPayload = new JWTPayload();

        jwtPayload.setId((Long) claims.get(JWTClaimKey.ID.getValue()));
        jwtPayload.setEmail((String) claims.get(JWTClaimKey.EMAIL.getValue()));
        jwtPayload.setUserName((String) claims.get(JWTClaimKey.USERNAME.getValue()));
        jwtPayload.setRole((String) claims.get(JWTClaimKey.ROLE.getValue()));

        return jwtPayload;
    }
}
