package com.lecuong.shop.validate;

import com.lecuong.shop.exception.ValidateException;
import com.lecuong.shop.model.request.user.UserSaveRequest;
import org.apache.commons.lang3.StringUtils;

public class UserValidate {

    public static UserSaveRequest validate(UserSaveRequest userSaveRequest) {
        return Validator.of(userSaveRequest, () -> new ValidateException("Object not null"))
                .validate(UserSaveRequest::getUserName, UserValidate::validUserName, () -> new ValidateException("user invalid"))
                .validate(UserSaveRequest::getPassword, UserValidate::validPassword, () -> new ValidateException("password invalid"))
                .validate(UserSaveRequest::getAddress, UserValidate::validAddress, () -> new ValidateException("address invalid"))
                .get();
    }

    private static boolean validUserName(String userName) {
        return StringUtils.isBlank(userName);
    }

    private static boolean validPassword(String password) {
        return StringUtils.isBlank(password);
    }

    private static boolean validAddress(String address) {
        return StringUtils.isBlank(address);
    }
}
