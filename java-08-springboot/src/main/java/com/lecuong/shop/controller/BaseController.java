package com.lecuong.shop.controller;

import com.lecuong.shop.exception.StatusTemplate;
import com.lecuong.shop.model.response.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class BaseController {

    public ResponseEntity<BaseResponse> success() {
        BaseResponse response = new BaseResponse(StatusTemplate.SUCCESS);
        return ResponseEntity.ok(response);
    }

    public ResponseEntity<BaseResponse> success(Object object) {
        return ResponseEntity.ok(new BaseResponse(StatusTemplate.SUCCESS, object));
    }
}
