package com.lecuong;

public class Main {

    /*
    * String Pool:
    *       + Là một vùng nhớ đặc biệt nằm trong vùng nhớ Heap, dùng để lưu trữ các biến được khai báo theo kiểu String.
    *       + Giúp tối ưu hóa việc lưu trữ và sử dụng vùng nhớ khi khai báo biến String, giúp hạn chế tình trạng tràn bộ nhớ Java Heap Space.
    */

    public static void main(String[] args) {

        String name = "cuonglm";
        String name1 = "cuonglm";
        String name2 = new String("cuonglm");

        System.out.println(name == name1);
        System.out.println(name == name2);
    }

}
