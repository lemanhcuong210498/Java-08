package com.lecuong.LocalDate;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate.toString());
    }
}
