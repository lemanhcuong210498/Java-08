package com.lecuong.FuntionalInterface;

public class Main {

    /*
        Trong FunctionalInterface:
            + Chỉ chứa duy nhất 1 abstract method
            + Mặc định khi chúng ta không đánh dấu interface đó là FunctionalInterface nhưng chúng ta lại chỉ có 1 abstract method duy nhất
                => JVM sẽ tự hiểu đấy là FuntionalInterface
            + Khi interface là FuntionalInterface thì chúng ta có thể sử dụng được biểu thức lambda và reference method.
            + Trong 1 Functional chỉ có duy nhất 1 abstract method nhưng có thể có nhiều phương thức default và static.
            + Trong FunctionalInterface không nhất thiết phải có abstract method nhưng nó phải extends 1 interface khác và interfce này có duy nhất 1 abstrcact method.

     */

    public static boolean check(String name, Demo<String> demo){
        return demo.test(name);
    }

    public static void main(String[] args) {
        boolean check1 = check("cuonglm", String::isEmpty);
        System.out.println(check1);

    }

}
