package com.lecuong.LambdaExpressions;

@FunctionalInterface
public interface Test {

    void test(int a, int b);
}
