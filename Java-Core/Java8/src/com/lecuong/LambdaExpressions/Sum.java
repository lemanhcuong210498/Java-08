package com.lecuong.LambdaExpressions;

@FunctionalInterface
public interface Sum {

    int sum(int a, int b);
}
