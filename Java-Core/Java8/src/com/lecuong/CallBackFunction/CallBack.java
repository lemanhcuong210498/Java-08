package com.lecuong.CallBackFunction;

public interface CallBack {

    void call(String s);
}
