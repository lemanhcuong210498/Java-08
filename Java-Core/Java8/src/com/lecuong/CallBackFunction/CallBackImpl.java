package com.lecuong.CallBackFunction;

public class CallBackImpl implements CallBack{

    @Override
    public void call(String s) {
        System.out.println("Call back function: " + s);
    }
}
