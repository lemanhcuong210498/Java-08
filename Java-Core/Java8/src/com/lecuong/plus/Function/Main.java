package com.lecuong.plus.Function;

import java.util.function.Function;

public class Main {

    public static void main(String[] args) {

        Function<String, Integer> function = (s) -> Integer.parseInt(s);
        System.out.println(function.apply("1"));
    }
}
