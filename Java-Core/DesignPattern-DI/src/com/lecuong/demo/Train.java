package com.lecuong.demo;

import com.lecuong.demo.ITravel;

public class Train implements ITravel {

    @Override
    public void move(){
        System.out.println("Travel by train");
    }

}
