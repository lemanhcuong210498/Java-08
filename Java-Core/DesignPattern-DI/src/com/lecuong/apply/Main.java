package com.lecuong.apply;

import com.lecuong.apply.controller.UserController;

public class Main {

    public static void main(String[] args) {

        /*Design pattern DI-Factory*/

        UserController userController = (UserController) ControllerFactory.createController(1,1);
        userController.save();
        userController.update();
    }
}
