package com.lecuong.apply;

import com.lecuong.apply.service.Service;
import com.lecuong.apply.service.impl.ProductService;
import com.lecuong.apply.service.impl.RoleService;
import com.lecuong.apply.service.impl.UserService;

public class ServiceFactory {

    public static Service getService(int serviceType){
        switch (serviceType){
            case 1:
                return new UserService();
            case 2:
                return new RoleService();
            case 3:
                return new ProductService();
        }
        return null;
    }

}
