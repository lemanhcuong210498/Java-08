package com.lecuong.apply.controller;

import com.lecuong.apply.service.impl.ProductService;

public class ProductController implements Controller {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    public void getById(long id){
        productService.getProductById(id);
    }
}
