package com.lecuong;

public class Main {

    /*
    * Interface:
    *       + ý nghĩa khi sử dụng interface nó chỉ bổ trợ về mặt chức năng.
    *       + tất cả các phương thức của interface mặc địng là public abstract.
    *       + trong interface không có biến mà chỉ có hằng số được khai báo là public static final.
    *       + 1 class muốn triển khai các chức năng của interface đó thì dùng từ khóa implements.
    *
    * Interface trong java 8:
    *       + ở trong java 7 thì mặc định các method là public, các method ko có thân hàm.
    *       + từ java 8 thì các method của interface có thể có thân hàm.
    *       + từ java 8 khi muốn viết thân hàm trong interface thì sử dụng từ khóa default và static, thì mặc định các class kế thừa nó được mặc định sử dụng.
    *       + với static trong interface người ta khuyến cáo chỉ dùng để khởi tạo đối tượng
    */

    public static void main(String[] args) {
        Java8 java8 = new Java8Impl();
        java8.printf();
    }

}
