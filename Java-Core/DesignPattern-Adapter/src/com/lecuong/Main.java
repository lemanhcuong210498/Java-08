package com.lecuong;

import com.lecuong.entity.Role;
import com.lecuong.entity.User;
import com.lecuong.repository.RoleRepository;
import com.lecuong.repository.UserRepository;
import com.lecuong.repository.impl.RoleRepositoryImpl;
import com.lecuong.repository.impl.UserRepositoryImpl;

public class Main {

    /*
    * Design Repository (DDD 3 design)
    * Adapter (gof 23 design)
    */



    public static void main(String[] args) {
        UserRepository userRepository = new UserRepositoryImpl();
        userRepository.save(new User());

        RoleRepository roleRepository = new RoleRepositoryImpl();
        roleRepository.save(new Role());
    }

}
