package com.lecuong;

public class Main {

    public static void main(String[] args) {

        //Inner Static Class
        InnerStaticClass.Inner innerStaticClass = new InnerStaticClass.Inner();
        innerStaticClass.print();

        //Inner Non Static Class
        InnerClass innerClass = new InnerClass();
        InnerClass.Inner in = innerClass.new Inner();
        innerClass.setName("cuong dep trai");
        in.print();
    }
}
