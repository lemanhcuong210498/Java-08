package com.lecuong.main;

import com.lecuong.overload.PhepCongHaiSo;
import com.lecuong.override.Child;
import com.lecuong.override.PerSon;

public class Main {

    public static void main(String[] args) {
        /*
        * Overload (nạp chồng):
        *       + Cho phép 1 lớp có khả năng định nghĩa ra nhiều phương thức có cùng tên nhưng khác nhau về tham số truyền vào.
        *       + Được sử dụng để giúp code của chương trình dễ đọc hơn.
        *       + Được thực hiện bên trong class.
        *       + Tham số truyền vào khác nhau, và cùng hoặc khác nhau kiểu trả về.
        *       + Nạp chồng là ví dụ đa hình lúc biên dịch.
        *
        *  Override (ghi đè):
        *       + Override là một tính năng cho phép một lớp con cung cấp một triển khai cụ thể của phương thức đã được cung cấp ở lớp cha.
        *       + Được sử dụng để cung cấp cài đặt cụ thể cho phương thức được khai báo ở lớp cha.
        *       + Xảy ra trong 2 class có quan hệ ké thừa.
        *       + Tham số truyền vào giống nhau và giống nhau kiểu trả về.
        *       + Là ví dụ đa hình lúc runtime.
        */

        //Overload:
        PhepCongHaiSo pchs = new PhepCongHaiSo();
        System.out.println("Tổng: " +pchs.add(1,2));
        System.out.println("Tổng: " +pchs.add(1.2f,2.2f));
        System.out.println("Tổng: " +pchs.add(1,2,3));

        //Override:
        PerSon child = new Child();
        child.nhap();
        child.hien();

    }

}
