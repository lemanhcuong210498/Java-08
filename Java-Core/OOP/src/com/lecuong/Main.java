package com.lecuong;

public class Main {

    /*
    * Ngôn ngữ lập trình thuần hướng đối tượng
    * Sơ khai: tất cả code đưa hết vào hàm main
    * Thủ tục: phân tách nghiệp vụ ra thành các hàm => được gọi trong main
    * OOP: cho phép chúng ta làm việc thông qua đối tượng
    *
    * Class:
    *       + là khuôn mẫu để tạo ra object.
    *       + được tạo thành bởi quá trình trìu tượng hóa 1 tập các đối tượng có chug tính chất ở ngoài cuộc sống vào trong lập trình.
    *
    * Object:
    *       + Object là đối tượng được tạo ra bởi class
    *       + Object có :
    *           Trạng thái(gồm các thuộc tính và giá trị của thuộc tính),
    *           Hành vi(các phương thức của đối tượng),
    *           Định danh(nó tên duy nhất).
    *
    * AccessModifier:
    *       + public, protected, default, private.
    *       + Phạm vi truy cập:
    *                        Trong class    |    Cùng package    |   Khác package    |   Khác package (cha-con)
    *           public             x        |         x          |        x          |        x
    *           protected          x        |         x          |                   |        x
    *           default            x        |         x          |                   |
    *           private            x        |                    |                   |
    *
    * 4 tính chất của OOP:
    *       + Tính trừu tượng: là tính chất ẩn chi tiết trình triển khai và chỉ hiện tính năng đến người dùng(hay nó là tính chất chỉ nêu tên vấn đề mà không hiển thị cụ thể).
    *       + Tính đóng gói (đóng gói các thuộc tính và phương thức chung của đối tượng vào 1 class
    *           && các class có chung tính chất vào 1 package
    *           && để che giấu thông tin của class đó với bên ngoài, không cho sửa chữa thuộc tính và hành vi của class
    *           ==> Tất cả thuộc tính để chế độ private).
    *       + Tính đa hình: là khả năng một đối tượng có thể  thực  hiện 1 tác vụ theo nhiều cách khác nhau.
    *       + Tính kế thừa: là khi mà 1 class nhận được các thuộc tính và phương thức của 1 class khác.
    *
    *  Constructor:
    *       + Được dùng để khởi tạo các giá trị ban đầu cho đối tượng.
    *
    *  Vùng nhớ trong java:
    *       + Stack:
    *           - Các biến và giá trị biến nguyên thủy.
    *           - Tham số của hàm.
    *           - Tên của các đối tượng.
    *       + Heap:
    *           - Giá trị của các object (vùng nhớ chứa object).
    *
    *
    */

    public static void main(String[] args) {

        Student std = new Student();
        std.setName("Cuong");
        std.setAge(22);
        std.setAddress("Nam Định");
        std.setSex("Nam");

        System.out.println(std.toString());

    }

}
