package com.lecuong.constant;

public class Config {

    public static String url;
    public static String username;
    public static String password;
    public static String driverClassName;
    public static int maxPoolSize;
    public static int connectionTimeOut;

}
