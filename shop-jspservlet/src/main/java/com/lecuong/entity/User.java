package com.lecuong.entity;

import com.lecuong.constant.annotation.Column;
import com.lecuong.constant.annotation.Entity;
import com.lecuong.constant.annotation.Id;

@Entity(value = "users")
public class User {

    @Id(autoIncrement = true, value = "id")
    private long id;

    @Column(value = "address")
    private String address;

    @Column(value = "phone")
    private String phone;

    public User() {
    }

    public User(long id, String address, String phone) {
        this.id = id;
        this.address = address;
        this.phone = phone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
