package com.lecuong.repository.impl;

import com.lecuong.connection.MySQLConnection;
import com.lecuong.constant.annotation.Id;
import com.lecuong.paging.Pageable;
import com.lecuong.repository.JpaRepository;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.stream.Stream;

import static com.lecuong.utils.ReflectionUtils.*;

public class BaseRepository<T, ID> implements JpaRepository<T, ID> {

    private Class<T> tClass;
    private String tableName;
    private static String insert = "INSERT INTO %s(%s) VALUES(%s)";

    public BaseRepository(){
        this.tClass = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.tableName = getClassName(tClass);
    }

    @Override
    public T save(T t) {

        StringBuilder attribute = new StringBuilder();
        StringBuilder values = new StringBuilder();
        Field[] fields = tClass.getDeclaredFields();
        boolean checkAutoIncrement = false;
        int index = 0;
        int count = 0;

        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)){
                index = count;
                checkAutoIncrement = autoIncrement(tClass, field.getName());
                if (checkAutoIncrement){
                    continue;
                }
                attribute.append(primaryName(tClass, field.getName())).append(",");
                values.append("?,");
                continue;
            }
            attribute.append(getColumnName(tClass, field.getName())).append(",");
            values.append("?,");
            count++;
        }

        String sql = String.format(insert, tableName, attribute.deleteCharAt(values.length() - 1), values.deleteCharAt(values.length() - 1));
        Connection connection = MySQLConnection.connection();

        try {
            connection.setAutoCommit(false);
            PreparedStatement ps = connection.prepareStatement(sql);
            int j = 0;
            for (int i = 0; i < fields.length; i++){
                if (i == index) {
                    if (checkAutoIncrement){
                        continue;
                    }
                    ps.setObject(j + 1, methodGet(t, fields[i].getName()));
                    j++;
                    continue;
                }
                ps.setObject(j + 1, methodGet(t, fields[i].getName()));
                j++;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    @Override
    public void update(ID id, T t) {

    }

    @Override
    public Optional<T> findById(ID id) {
        return Optional.empty();
    }

    @Override
    public void delete(ID id) {

    }

    @Override
    public Stream<T> findAll() {
        return null;
    }

    @Override
    public Stream<T> findAll(Pageable pageable) {
        return null;
    }
}
