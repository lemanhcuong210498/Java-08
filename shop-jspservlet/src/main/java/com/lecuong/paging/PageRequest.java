package com.lecuong.paging;

public class PageRequest implements Pageable{

    private int pageIndex;
    private int pageSize;

    private PageRequest(int pageIndex, int pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    public static PageRequest of(final int pageIndex,final int pageSize){
        return  new PageRequest(pageIndex, pageSize);
    }

    @Override
    public int getIndex() {
        return 0;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public int getOffset() {
        return 0;
    }
}
